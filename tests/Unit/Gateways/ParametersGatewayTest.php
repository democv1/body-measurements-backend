<?php


namespace Tests\Unit\Gateways;


use App\Repositories\V1\ParametersRepository;
use App\Repositories\V1\UsersRepository;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ParametersGatewayTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private $gateway = 'App\Gateways\V1\ParametersGateway';

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class(): void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->assertInstanceOf($this->gateway, $gateway);
    }

    /**
     * Test can index
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function test_can_index():void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->index();
        $this->assertEmpty($records);
        $this->seed(DatabaseSeeder::class);
        $records = $gateway->index();
        $this->assertTrue(count($records) === 1);
    }

    /**
     * Test cannot store if validator fails
     * @throws BindingResolutionException
     */
    public function test_cannot_store()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->seed(DatabaseSeeder::class);

        $payload = [
            'user_id' => 1,
        ];
        $errors = $gateway->store($payload);
        $errors = $errors->getMessages();
        $this->assertEquals('Parameters need to be unique for single user.', $errors['user_id'][0]);
    }

    public function test_can_store()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->seed(DatabaseSeeder::class);
        $repository = new ParametersRepository();
        $repository->destroy(1);
        $payload = [
            'user_id' => 1,
            'height' => 180,
            'birthdate' => '31-12-1980',
        ];
        $store = $gateway->store($payload);
        $this->assertTrue($store);
    }

    /**
     * Test can index
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function test_can_index_by_user():void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->index(1);
        $this->assertEmpty($records);
        $this->seed(DatabaseSeeder::class);

        $payload = [
            'email' => '2@example.com',
            'password' => Hash::make('password'),
        ];
        UsersRepository::factory()->create($payload);

        $payload = [
            'user_id' => 2,
            'height' => 200,
            'birthdate' => '2001-01-01',
            'sex' => 'f',
        ];
        ParametersRepository::factory()->create($payload);

        $records = $gateway->index(1);
        $this->assertTrue(count($records) === 1);
    }

    /**
     * Test can store without user id
     *
     * @return void
     */
    public function test_can_store_without_user_id()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->seed(DatabaseSeeder::class);
        $repository = new ParametersRepository();
        $repository->destroy(1);

        $user = UsersRepository::first();
        auth()->login($user);

        $payload = [
            'height' => 180,
            'birthdate' => '31-12-1980',
        ];
        $store = $gateway->store($payload);
        $this->assertTrue($store);
    }
}
