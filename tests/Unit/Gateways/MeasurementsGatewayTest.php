<?php


namespace Tests\Unit\Gateways;


use App\Repositories\V1\UsersRepository;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MeasurementsGatewayTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private $gateway = 'App\Gateways\V1\MeasurementsGateway';

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class(): void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->assertInstanceOf($this->gateway, $gateway);
    }

    /**
     * Test can index
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function test_can_index():void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->index();
        $this->assertTrue(count($records) === 0);
        $this->seed(DatabaseSeeder::class);
        $records = $gateway->index();
        $this->assertTrue(count($records) === 1);
    }

    public function test_can_store_with_user_id()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->seed(DatabaseSeeder::class);
        $payload = [
            'user_id' => 1,
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $store = $gateway->store($payload);
        $this->assertTrue($store);
    }

    /**
     * Test can store without user id
     *
     * @return void
     */
    public function test_can_store_without_user_id()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->seed(DatabaseSeeder::class);

        $user = UsersRepository::first();
        auth()->login($user);

        $payload = [
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $store = $gateway->store($payload);
        $this->assertTrue($store);
    }

    /**
     * Test can delete
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function test_can_delete():void
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->index();
        $this->assertCount(1, $records);
        $delete = $gateway->delete(1);
        $this->assertTrue($delete);
        $records = $gateway->index();
        $this->assertCount(0, $records);
    }
}
