<?php

namespace Tests\Unit\Gateways;

use Tests\TestCase;

class PingGatewayTest extends TestCase
{
    private $gateway = 'App\Gateways\V1\PingGateway';

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->assertInstanceOf($this->gateway, $gateway);
    }

    /**
     * Test index returns array
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $payload = [
            'app_version' => '9.8.7',
        ];
        $gateway = $this->app->make($this->gateway);
        $this->assertEquals($payload, $gateway->index());
    }
}
