<?php


namespace Tests\Unit\Gateways;


use App\Repositories\V1\UsersRepository;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UsersGatewayTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private $gateway = 'App\Gateways\V1\UsersGateway';

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class(): void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $this->assertInstanceOf($this->gateway, $gateway);
    }

    /**
     * Test can index
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function test_can_index():void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->index();
        $this->assertTrue(count($records) === 0);
        $this->seed(DatabaseSeeder::class);
        $records = $gateway->index();
        $this->assertTrue(count($records) === 1);
    }

    /**
     * Test can show
     *
     * @return void
     */
    public function test_can_show():void
    {
        $this->withoutExceptionHandling();
        $gateway = $this->app->make($this->gateway);
        $records = $gateway->show(1);
        $this->assertNull($records);
        $this->seed(DatabaseSeeder::class);
        $records = $gateway->show(1);
        $this->assertEquals(1, $records->id);

        $payload = [
            'email' => '2@example.com',
            'password' => Hash::make('password'),
            'role_id' => null,
        ];
        UsersRepository::factory()->create($payload);
        $records = $gateway->show(2);
        $this->assertEquals(2, $records->id);

    }
}
