<?php

namespace Tests\Unit\Middlewares;

use App\Http\Middleware\JsonResponseMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\ResponseFactory;
use Tests\TestCase;

class JsonResponseTest extends TestCase
{
    public function test_response()
    {
        $response = new Response(
            "foo",
            200,
            ['foo' => 'bar'],
        );

        $request = Request::create('/', 'GET');

        $middleware = $this->app->make(JsonResponseMiddleware::class);

        $result = $middleware->handle($request, function () use ($response) {
            return $response;
        });

        $this->assertSame('foo', $result->getData());

    }
}
