<?php

namespace Tests\Unit\Repositories;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Repositories\V1\MeasurementsRepository;

class MeasurementsRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $repository = new MeasurementsRepository();
        $this->assertInstanceOf('App\Repositories\V1\MeasurementsRepository', $repository);
    }

    /**
     * Test index without data
     *
     * @return void
     */
    public function test_index_without_data()
    {
        $this->withoutExceptionHandling();
        $repository = new MeasurementsRepository();
        $items = $repository->index();
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $items);
        $this->assertCount(0, $items);
    }

    /**
     * Test index with data
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new MeasurementsRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $this->assertSame(1, $items[0]->id);
        $this->assertEquals(40.01, $items[0]->hip); //fianchi
        $this->assertEquals(50.02, $items[0]->waist); // vita
        $this->assertEquals(60.03, $items[0]->neck); // collo
        $this->assertEquals(70.04, $items[0]->knee); // coscia
        $this->assertEquals(80.05, $items[0]->arm); // braccio
        $this->assertEquals(90.06, $items[0]->weight); // peso
        $this->assertSame('info@example.com', $items[0]->users->email);
    }

    /**
     * Test can store
     *
     * @return void
     */
    public function test_can_store()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new MeasurementsRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $payload = [
            'user_id' => 1,
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $repository->store($payload);
        $items = $repository->index();
        $this->assertCount(2, $items);
        $this->assertEquals(1, $items[0]->user_id);
        $this->assertEquals(1, $items[1]->user_id);
        $this->assertEquals('info@example.com', $items[0]->users->email);
        $this->assertEquals('info@example.com', $items[1]->users->email);
        $this->assertEquals(70.99, $items[1]->hip);
        $this->assertEquals(71.99, $items[1]->waist);
        $this->assertEquals(72.99, $items[1]->neck);
        $this->assertEquals(73.99, $items[1]->knee);
        $this->assertEquals(74.99, $items[1]->arm);
        $this->assertEquals(75.99, $items[1]->weight);

    }

    /**
     * Test can delete
     *
     * @return void
     */
    public function test_can_delete()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new MeasurementsRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $repository->delete_item(1);
        $items = $repository->index();
        $this->assertCount(0, $items);
    }
}
