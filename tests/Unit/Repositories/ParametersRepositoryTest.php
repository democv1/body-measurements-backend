<?php

namespace Tests\Unit\Repositories;

use App\Repositories\V1\UsersRepository;
use Carbon\Carbon;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

use App\Repositories\V1\ParametersRepository;

class ParametersRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $repository = new ParametersRepository();
        $this->assertInstanceOf('App\Repositories\V1\ParametersRepository', $repository);
    }

    /**
     * Test index without data
     *
     * @return void
     */
    public function test_index_without_data()
    {
        $this->withoutExceptionHandling();
        $repository = new ParametersRepository();
        $items = $repository->index();
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $items);
        $this->assertCount(0, $items);
    }

    /**
     * Test index with data
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new ParametersRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $this->assertSame(1, $items[0]->id);
        $this->assertEquals('m', $items[0]->sex);
        $this->assertEquals('31-12-1970', $items[0]->birthdate);
        $this->assertEquals(175.12, $items[0]->height);
        $this->assertSame('info@example.com', $items[0]->users->email);
    }

    /**
     * Test index with age
     *
     * @return void
     */
    public function test_index_with_age()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new ParametersRepository();

        $items = $repository->index();
        $current_age = Carbon::parse($items[0]->birthdate)->diff(Carbon::now())->y;
        $this->assertEquals($current_age, $items[0]->age);
    }

    /**
     * Test can store
     *
     * @return void
     */
    public function test_can_store()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new ParametersRepository();
        $repository->destroy(1);
        $items = $repository->index();
        $this->assertCount(0, $items);
        $payload = [
            'user_id' => 1,
            'birthdate' => '1970-12-31',
            'height' => 170.98
        ];
        $repository->store($payload);
        $items = $repository->index();
        $this->assertCount(1, $items);
        $this->assertEquals(2, $items[0]->id);
        $this->assertEquals('info@example.com', $items[0]->users->email);
        $this->assertEquals(170.98, $items[0]->height);
        $current_age = Carbon::parse($items[0]->birthdate)->diff(Carbon::now())->y;
        $this->assertEquals($current_age, $items[0]->age);

    }

    /**
     * Test can index by user
     */
    public function test_index_by_user()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);

        $payload = [
            'email' => '2@example.com',
            'password' => Hash::make('password'),
        ];
        UsersRepository::factory()->create($payload);

        $payload = [
            'user_id' => 2,
            'height' => 200,
            'birthdate' => '2001-01-01',
            'sex' => 'f',
        ];
        ParametersRepository::factory()->create($payload);

        $repository = new ParametersRepository();
        $items = $repository->index(2);
        $this->assertCount(1, $items);
        $this->assertSame(2, $items[0]->id);
        $this->assertEquals('f', $items[0]->sex);
        $this->assertEquals('2001-01-01', $items[0]->birthdate);
        $this->assertEquals(200.00, $items[0]->height);
        $this->assertSame('2@example.com', $items[0]->users->email);
    }
}
