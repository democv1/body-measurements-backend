<?php

namespace Tests\Unit\Repositories;

use Tests\TestCase;

use App\Repositories\V1\PingRepository;

class PingRepositoryTest extends TestCase
{
    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $repository = new PingRepository();
        $this->assertInstanceOf('App\Repositories\V1\PingRepository', $repository);
    }

    /**
     * Test index return array
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $payload = [
            'app_version' => '9.8.7',
        ];
        $repository = new PingRepository();
        $this->assertEquals($payload, $repository->index());
    }
}
