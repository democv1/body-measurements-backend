<?php

namespace Tests\Unit\Repositories;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

use App\Repositories\V1\UsersRepository;

class UsersRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $repository = new UsersRepository();
        $this->assertInstanceOf('App\Repositories\V1\UsersRepository', $repository);
    }

    /**
     * Test index without data
     *
     * @return void
     */
    public function test_index_without_data()
    {
        $this->withoutExceptionHandling();
        $repository = new UsersRepository();
        $items = $repository->index();
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $items);
        $this->assertCount(0, $items);
    }

    /**
     * Test index with data
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new UsersRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $this->assertEquals(1, $items[0]->id);
        $this->assertEquals('info@example.com', $items[0]->email);
        $this->assertEquals('admin', $items[0]->roles->role);
    }

    /**
     * Test show
     *
     * @return void
     */
    public function test_show()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new UsersRepository();
        $items = $repository->show(1);
        $this->assertEquals(1, $items->id);
        $this->assertEquals('info@example.com', $items->email);
        $this->assertEquals('admin', $items->roles->role);

        $payload = [
            'email' => '2@example.com',
            'password' => Hash::make('password'),
            'role_id' => null,
        ];
        UsersRepository::factory()->create($payload);

        $items = $repository->show(2);
        $this->assertEquals(2, $items->id);
        $this->assertEquals('2@example.com', $items->email);
        $this->assertNull($items->roles);

    }
}
