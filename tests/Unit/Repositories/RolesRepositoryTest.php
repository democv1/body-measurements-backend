<?php

namespace Tests\Unit\Repositories;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Repositories\V1\RolesRepository;

class RolesRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test can instantiate class
     *
     * @return void
     */
    public function test_can_instantiate_class()
    {
        $this->withoutExceptionHandling();
        $repository = new RolesRepository();
        $this->assertInstanceOf('App\Repositories\V1\RolesRepository', $repository);
    }

    /**
     * Test index without data
     *
     * @return void
     */
    public function test_index_without_data()
    {
        $this->withoutExceptionHandling();
        $repository = new RolesRepository();
        $items = $repository->index();
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $items);
        $this->assertCount(0, $items);
    }

    /**
     * Test index with data
     *
     * @return void
     */
    public function test_index()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $repository = new RolesRepository();
        $items = $repository->index();
        $this->assertCount(1, $items);
        $this->assertSame(1, $items[0]->id);
        $this->assertSame('admin', $items[0]->role);
    }
}
