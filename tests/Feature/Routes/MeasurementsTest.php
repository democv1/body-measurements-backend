<?php

namespace Tests\Feature\Routes;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MeasurementsTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test cannot index without token
     *
     * @return void
     */
    public function test_cannot_index_without_token()
    {
        $request = $this->json(
            'GET',
            Route('measurements_index')
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test can index
     *
     * @return void
     */
    public function test_can_index()
    {
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('measurements_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(1, $data->data[0]->id);
        $this->assertSame('info@example.com', $data->data[0]->users->email);
    }

    /**
     * Test cannot store without token
     *
     * @return void
     */
    public function test_cannot_store_without_token()
    {
        $payload = [
            'user_id' => 1,
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $request = $this->json(
            'POST',
            Route('measurements_store'),
            $payload,
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test can store
     *
     * @return void
     */
    public function test_can_store()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $payload = [
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'POST',
                Route('measurements_store'),
                $payload,
            );
        $request->assertStatus(201);
        $request->assertJsonFragment(
            [
                'success' => 'created',
            ]
        );

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json(
                'GET',
                Route('measurements_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertJsonFragment([
            'total' => 2,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(2, $data->data[0]->id);
    }

    /**
     * Test can sort
     *
     * @return void
     */
    public function test_can_sort()
    {
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $payload = [
            'hip' => 70.99,
            'waist' => 71.99,
            'neck' => 72.99,
            'knee' => 73.99,
            'arm' => 74.99,
            'weight' => 75.99
        ];
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'POST',
                Route('measurements_store'),
                $payload,
            );
        $request->assertStatus(201);

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('measurements_index'),
                [
                    'sort' => 'created_at',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 2,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(2, $data->data[0]->id);
        $this->assertSame('75.99', $data->data[0]->weight);
        $this->assertSame('info@example.com', $data->data[0]->users->email);
    }

    /**
     * Test cannot delete without token
     *
     * @return void
     */
    public function test_cannot_delete_without_token()
    {
        $this->seed(DatabaseSeeder::class);
        $request = $this->json(
                'DELETE',
                Route('measurements_delete',
                    [
                        'id' => 1
                    ]
                )
            );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);

    }

    /**
     * Test can delete
     *
     * @return void
     */
    public function test_can_delete()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json(
                'DELETE',
                Route('measurements_delete',
                    [
                        'id' => 1
                    ]
                )
            );

        $request->assertStatus(200);
        $request->assertJsonFragment(
            [
                'success' => 'deleted',
            ]
        );

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('measurements_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 0,
            'per_page' => 15
        ]);
    }

    /**
     * Test cannot delete if id is wrong
     *
     * @return void
     */
    public function test_cannot_delete_if_id_is_wrong()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json(
                'DELETE',
                Route('measurements_delete',
                    [
                        'id' => 2
                    ]
                )
            );

        $request->assertStatus(400);
        $request->assertJsonFragment(
            [
                'error' => 'Unable to delete',
            ]
        );

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('measurements_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);
    }
}
