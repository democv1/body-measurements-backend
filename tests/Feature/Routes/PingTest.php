<?php

namespace Tests\Feature\Routes;

use Tests\TestCase;

class PingTest extends TestCase
{
    /**
     * Test can index
     *
     * @return void
     */
    public function test_can_index()
    {
        $this->withoutExceptionHandling();
        $request = $this->json(
            'GET',
            Route('ping_index')
        );
        $request->assertJsonStructure(
            [
                'app_version',
            ]
        );
        $data = $request->getOriginalContent();
        $this->assertSame('9.8.7', $data['app_version']);

    }
}
