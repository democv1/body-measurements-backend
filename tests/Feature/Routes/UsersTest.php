<?php

namespace Tests\Feature\Routes;

use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test cannot index without token
     *
     * @return void
     */
    public function test_cannot_index_without_token()
    {
        $request = $this->json(
            'GET',
            Route('users_index')
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test can index
     *
     * @return void
     */
    public function test_can_index()
    {
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('users_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(1, $data->data[0]->id);
        $this->assertSame('info@example.com', $data->data[0]->email);
    }

    /**
     * Test cannot show without token
     *
     * @return void
     */
    public function test_cannot_show_without_token()
    {
        $request = $this->json(
            'GET',
            Route('users_show',
                [
                    'id' => 1,
                ]
            )
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test can show
     *
     * @return void
     */
    public function test_can_show()
    {
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('users_show',
                    [
                        'id' => 1,
                    ]
                )
            );
        $request->assertStatus(200);
        $data = $request->getData();
        $this->assertEquals(1,$data->id);
        $this->assertEquals('admin', $data->roles->role);
    }

    /**
     * Test can show
     *
     * @return void
     */
    public function test_users_me()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('users_me')
            );
        $request->assertStatus(200);
        $data = $request->getData();
        $this->assertEquals(1,$data->id);
        $this->assertEquals('info@example.com',$data->email);
        $this->assertEquals('admin', $data->roles->role);
    }
}
