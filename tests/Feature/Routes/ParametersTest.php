<?php

namespace Tests\Feature\Routes;

use App\Repositories\V1\ParametersRepository;
use App\Repositories\V1\UsersRepository;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ParametersTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * Test cannot index without token
     *
     * @return void
     */
    public function test_cannot_index_without_token()
    {
        $request = $this->json(
            'GET',
            Route('parameters_index')
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test can index
     *
     * @return void
     */
    public function test_can_index()
    {
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('parameters_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(1, $data->data[0]->id);
        $this->assertSame('info@example.com', $data->data[0]->users->email);
    }

    /**
     * Test cannot store without token
     *
     * @return void
     */
    public function test_cannot_store_without_token()
    {
        $payload = [
            'place_id' => 1,
            'user_id' => 1,
            'utility_id' => 1,
            'consumption' => 6554
        ];
        $request = $this->json(
            'POST',
            Route('parameters_store'),
            $payload,
        );
        $request->assertJsonStructure(
            [
                'error',
            ]
        );
        $request->assertStatus(401);
        $request->assertJsonFragment([
            'error' => 'Unauthenticated',
        ]);
    }

    /**
     * Test cannot store if validator fails
     *
     * @return void
     */
    public function test_cannot_store_if_validator_fails()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $payload = [
            'user_id' => 1,
        ];
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'POST',
                Route('parameters_store'),
                $payload,
            );
        $request->assertStatus(400);
        $request->assertJsonFragment(
            [
                'user_id' => [
                    'Parameters need to be unique for single user.'
                ],
            ]
        );
    }

    /**
     * Test can store
     *
     * @return void
     */
    public function test_can_store()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $repository = new ParametersRepository();
        $repository->destroy(1);

        $payload = [
            'user_id' => 1,
            'height' => 170,
            'birthdate' => '1980-11-22',
            'sex' => 'm',
        ];
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'POST',
                Route('parameters_store'),
                $payload,
            );
        $request->assertStatus(201);
        $request->assertJsonFragment(
            [
                'success' => 'created',
            ]
        );

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json(
                'GET',
                Route('parameters_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertSame(2, $data->data[0]->id);
        $this->assertSame('m', $data->data[0]->sex);
    }

    /**
     * Test can index
     *
     * @return void
     */
    public function test_can_index_by_user()
    {
        $this->seed(DatabaseSeeder::class);

        $payload = [
            'email' => '2@example.com',
            'password' => Hash::make('password'),
        ];
        UsersRepository::factory()->create($payload);

        $payload = [
            'user_id' => 2,
            'height' => 200,
            'birthdate' => '2001-01-01',
            'sex' => 'f',
        ];
        ParametersRepository::factory()->create($payload);

        $token = auth()->tokenById(1);
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'GET',
                Route('parameters_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertStatus(200);
        $request->assertJsonStructure(
            [
                'current_page',
                'data',
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'per_page',
                'prev_page_url',
                'to',
                'total',
            ]
        );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);

        $data = $request->getData();
        $this->assertCount(1, $data->data);
        $this->assertSame(1, $data->data[0]->id);
        $this->assertSame('info@example.com', $data->data[0]->users->email);
    }

    /**
     * Test multiple store with same user update parameters
     *
     * @return void
     */
    public function test_multiple_stores_with_same_user_update_parameters()
    {
        $this->withoutExceptionHandling();
        $this->seed(DatabaseSeeder::class);
        $token = auth()->tokenById(1);

        $payload = [
            'height' => '99',
            'birthdate' => '31-12-2001',
            'sex' => 'f',
        ];
        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer '.$token,
            ])
            ->json(
                'POST',
                Route('parameters_store'),
                $payload,
            );
        $request->assertStatus(201);
        $request->assertJsonFragment(
            [
                'success' => 'created',
            ]
        );

        $request = $this
            ->withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])
            ->json(
                'GET',
                Route('parameters_index'),
                [
                    'sort' => 'id',
                    'direction' => 'desc'
                ]
            );
        $request->assertJsonFragment([
            'total' => 1,
            'per_page' => 15
        ]);
    }
}
