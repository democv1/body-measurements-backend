<?php

namespace Database\Seeders;

use App\Repositories\V1\ParametersRepository;
use App\Repositories\V1\MeasurementsRepository;
use App\Repositories\V1\RolesRepository;
use App\Repositories\V1\UsersRepository;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        RolesRepository::factory()->create();
        UsersRepository::factory()->create();
        MeasurementsRepository::factory()->create();
        ParametersRepository::factory()->create();
    }
}
