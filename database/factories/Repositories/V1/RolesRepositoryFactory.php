<?php

namespace Database\Factories\Repositories\V1;

use App\Repositories\V1\RolesRepository;
use Illuminate\Database\Eloquent\Factories\Factory;

class RolesRepositoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RolesRepository::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'role' => 'admin',
        ];
    }
}
