<?php

namespace Database\Factories\Repositories\V1;

use App\Repositories\V1\ParametersRepository;
use Illuminate\Database\Eloquent\Factories\Factory;

class ParametersRepositoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ParametersRepository::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sex' => 'm',
            'birthdate' => '31-12-1970',
            'height' => 175.12,
            'user_id' => 1,
        ];
    }
}
