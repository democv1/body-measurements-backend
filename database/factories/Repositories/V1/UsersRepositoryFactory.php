<?php

namespace Database\Factories\Repositories\V1;

use App\Repositories\V1\UsersRepository;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsersRepositoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UsersRepository::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => 'info@example.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role_id' => 1,
        ];
    }
}
