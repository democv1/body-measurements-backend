<?php

namespace Database\Factories\Repositories\V1;

use App\Repositories\V1\MeasurementsRepository;
use Illuminate\Database\Eloquent\Factories\Factory;

class MeasurementsRepositoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MeasurementsRepository::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hip' => 40.01,
            'waist' => 50.02,
            'neck' => 60.03,
            'knee' => 70.04,
            'arm' => 80.05,
            'weight' => 90.06,
            'user_id' => 1,
            'created_at' => '2021-01-01',
            'updated_at' => '2021-01-01',
        ];
    }
}
