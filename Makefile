build:
	docker build --tag registry.heroku.com/body-measurement-backend/web --build-arg APP_VERSION=0.1.0 --file docker/app/Dockerfile "."
test:
	docker run -d --name imagetest -e "PORT=9876" -p 9876:9876 registry.heroku.com/body-measurement-backend/web
	docker exec -it imagetest php -i | grep "PHP Version => 8.0.11"
	docker container stop imagetest
	docker container rm imagetest
deploy:
	heroku config:set APP_VERSION=0.1.0 -a body-measurement-backend
	heroku container:login
	docker push registry.heroku.com/body-measurement-backend/web
	heroku container:release web -a body-measurement-backend
	heroku run php /var/www/artisan migrate --force -a body-measurement-backend
	heroku labs:enable -a body-measurement-backend runtime-new-layer-extract
sonar:
	sudo chown sineverba:sineverba coverage/clover.xml
	sudo sed -i -e 's,/data,/usr/src,g' sonar-project.properties
	sudo sed -i -e 's,/data,/usr/src,g' coverage/clover.xml
	docker run --rm -e SONAR_HOST_URL="http://192.168.1.104:9000" -e SONAR_LOGIN="101d41b3c43c52d4d377562063f112b98067bc42" \
	-v "/home/sineverba/sviluppo/personali/body-measurement-backend:/usr/src" sonarsource/sonar-scanner-cli:4.6
	sudo sed -i -e 's,/usr/src,/data,g' sonar-project.properties
	sudo sed -i -e 's,/usr/src,/data,g' coverage/clover.xml
swagger:
	sudo chown sineverba:sineverba storage/api-docs/api-docs.json
	docker-compose exec app php artisan l5-swagger:generate
