Body Measurement Backend
========================

> Body measurement backend project

Store all body measurement.

This project is composed by several parts:

| Feature | Link | Demo |
| ------- | ---- | ---- |
| Backend | [Github](https://github.com/sineverba/body-measurement-backend) | [Link](https://body-measurement-backend.herokuapp.com) |
| Frontend | [Github](https://github.com/sineverba/body-measurement-frontend) | [Link](https://body-measurement.netlify.app) |

| CI - CD - Coverage | Status |
| ------------------ | ------ |
| Semaphore CI | [![Build Status](https://sineverba.semaphoreci.com/badges/body-measurement-backend.svg)](https://sineverba.semaphoreci.com/projects/body-measurement-backend) |
| Circle CI | [![CircleCI](https://circleci.com/gh/sineverba/body-measurement-backend/tree/develop.svg?style=svg)](https://circleci.com/gh/sineverba/body-measurement-backend/tree/develop) |
| Coveralls | [![Coverage Status](https://coveralls.io/repos/github/sineverba/body-measurement-backend/badge.svg?branch=master)](https://coveralls.io/github/sineverba/body-measurement-backend?branch=master)|
| Codecov | [![codecov](https://codecov.io/gh/sineverba/body-measurement-backend/branch/master/graph/badge.svg)](https://codecov.io/gh/sineverba/body-measurement-backend) |
| SonarCloud | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sineverba_body-measurement-backend&metric=alert_status)](https://sonarcloud.io/dashboard?id=sineverba_body-measurement-backend) |
| Scrutinizer | [![Build Status](https://scrutinizer-ci.com/g/sineverba/body-measurement-backend/badges/build.png?b=master)](https://scrutinizer-ci.com/g/sineverba/body-measurement-backend/build-status/master) |
| Scrutinizer Quality Score | [![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/sineverba/body-measurement-backend/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/sineverba/body-measurement-backend/?branch=master)|

If you like this project or use it, **Star it!**. Your stars motivate developers to continue to develop it.

## Swagger documentation

`https://body-measurement-backend.herokuapp.com`


## Development
1. `$ cp env.example .env`
2. `$ php artisan jwt:secret`
3. `$ docker-compose up -d`
4. Connect to `http://localhost:8000`
