# 0.9.0
+ Upgrade dependencies
+ Add delete measurement

## 0.8.0
+ Add store measurements
+ Add sort by created at for measurements

## 0.7.3
+ Fix missing store birthdate

## 0.7.2
+ Fix missing sex store

## 0.7.1
+ Fix store multiple parameters for same user

## 0.7.0
+ Add store parameters
+ Get parameters only by logged user
+ Add users show
+ Add users me route

## 0.6.0
+ Add Measurements
+ Force all returns in JSON
+ Add parameters
+ Add Scrutinizer and StyleCI

## 0.5.0
+ Update dependencies
+ Add refresh token
+ Move coverage to 100

## 0.4.1
+ Fix `Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes`

## 0.4.0
+ Add login route

## 0.3.1
+ Fix missing JWT key

## 0.3.0
+ Refactor User repository. Add Traits Sort and Paginate
+ Add badges
+ Add JWT
+ Add Roles
+ Add Roles to Users

## 0.2.1
+ Fix error with Semaphore name

## 0.2.0
+ Add Docker deploy for production
+ Configure MySql - ClearDB
+ Add ping route
+ Add swagger
+ Remove `Sanctum` and useless migrations

## 0.1.0
+ First version
+ Add docker-compose setup
+ Add CI - CD
