<?php

/**
 * Ping Repository.
 *
 * Mantains all data for ping (app version)
 */

namespace App\Repositories\V1;

use App\Interfaces\V1\PingInterface;

/**
 * @OA\Schema(
 *   schema="PingSchema",
 *   title="Ping",
 *   description="Ping model",
 *   @OA\Property(
 *      property="app_version", description="Backend current version", type="string", example="1.0.0"
 *   ),
 * )
 */

class PingRepository implements PingInterface
{
    public function index()
    {
        $data = array();
        $data['app_version'] = env('APP_VERSION', '0.0.1');
        return $data;
    }
}
