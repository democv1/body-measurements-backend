<?php

namespace App\Repositories\V1;

use App\Interfaces\V1\MeasurementsInterface;
use App\Interfaces\V1\ParametersInterface;
use Carbon\Carbon;

/**
 * @OA\Schema(
 *   schema="ParametersSchema",
 *   title="Parameters data",
 *   description="Parameters model",
 *   @OA\Property(
 *          property="data",
 *          description="List of parameters",
 *          type="array",
 *          @OA\Items(
 *               @OA\Property(
 *                  property="id", description="ID of the data", type="number", example=1
 *               ),
 *               @OA\Property(
 *                  property="birthday", description="Date of birth", type="date", example="1970-12-31"
 *               ),
 *               @OA\Property(
 *                  property="height", description="Height", type="decimal", example="175.12"
 *               ),
 *               @OA\Property(
 *                  property="age", description="Age", type="numeric", example="40"
 *               ),
 *              @OA\Property(
 *                  property="users",
 *                  description="Associated users",
 *                  type="array",
 *                   @OA\Items(
 *                       @OA\Property(
 *                           property="id", description="ID of the user", type="number", example=1
 *                       ),
 *                       @OA\Property(
 *                           property="email", description="Email", type="string", example="info@example.com"
 *                       ),
 *                   ),
 *              )
 *        ),
 *   ),
 * )
 */

class ParametersRepository extends BaseRepository implements ParametersInterface
{

    /**
     * The name of the table
     *
     * @var string
     */
    protected $table = "parameters";

    /**
     * The sortable columns
     */
    public $sortables = [
        'id',
        'birthdate',
        'height',
        'sex',
    ];

    /**
     * The attribute mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'birthdate',
        'height',
        'sex',
    ];

    protected $appends = [
        'age',
    ];

    /**
     * Return associated users
     *
     */
    public function users()
    {
        return $this->hasOne(UsersRepository::class, 'id', 'user_id');
    }

    /**
     * Return current age
     */
    public function getAgeAttribute()
    {
        return Carbon::parse($this->birthdate)->diff(Carbon::now())->y;
    }

    /**
     * Return data
     */
    public function index($user_id = null)
    {
        if ($user_id) {
            return $this
                ->where('user_id', '=', $user_id)
                ->with('users')
                ->sort(request())
                ->paginate($this->getPerPage());
        }
        return $this
            ->with('users')
            ->sort(request())
            ->paginate($this->getPerPage());
    }
}
