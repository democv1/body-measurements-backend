<?php

namespace App\Repositories\V1;

use App\Interfaces\V1\UsersInterface;
use App\Traits\Paginate;
use App\Traits\Sort;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema(
 *   schema="UsersSchema",
 *   title="Users",
 *   description="Users model",
 *   @OA\Property(
 *          property="data",
 *          description="List of users",
 *          type="array",
 *          @OA\Items(
 *               @OA\Property(
 *                  property="id", description="ID of the user", type="number", example=1
 *               ),
 *               @OA\Property(
 *                  property="email", description="Email", type="string", example="info@example.com"
 *               ),
 *              @OA\Property(
 *                  property="roles",
 *                  description="Associated roles",
 *                  type="array",
 *                   @OA\Items(
 *                       @OA\Property(
 *                           property="id", description="ID of the role", type="number", example=1
 *                       ),
 *                       @OA\Property(
 *                           property="role", description="Role name", type="string", example="admin"
 *                       ),
 *                   ),
 *              )
 *        ),
 *   ),
 * )
 *
 * @OA\Schema(
 *   schema="UserShowSchema",
 *   title="Single user",
 *   description="Single user",
 *   @OA\Property(
 *      property="id", description="ID of the user", type="number", example=1
 *   ),
 *   @OA\Property(
 *      property="email", description="Email", type="string", example="info@example.com"
 *   ),
 *   @OA\Property(
 *       property="roles",
 *       description="Associated roles",
 *       type="array",
 *        @OA\Items(
 *            @OA\Property(
 *                property="id", description="ID of the role", type="number", example=1
 *            ),
 *            @OA\Property(
 *                property="role", description="Role name", type="string", example="admin"
 *            ),
 *        ),
 *   )
 * )
 */

class UsersRepository extends Authenticatable implements UsersInterface, JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes, Paginate, Sort;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The name of the table
     *
     * @var string
     */
    protected $table = "users";

    /**
     * Return associated roles
     *
     */
    public function roles()
    {
        return $this->hasOne(RolesRepository::class, 'id', 'role_id');
    }

    /**
     * Return data
     */
    public function index()
    {
        return $this
            ->with('roles')
            ->sort(request())
            ->paginate($this->getPerPage());
    }

    /**
     * Get single data
     * @return mixed
     */
    public function show($id)
    {
        return $this
            ->with('roles')
            ->where('id', '=', $id)
            ->first();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
