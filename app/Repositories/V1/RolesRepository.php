<?php

namespace App\Repositories\V1;

use App\Interfaces\V1\RolesInterface;

/**
 * @OA\Schema(
 *   schema="RolesSchema",
 *   title="Roles",
 *   description="Roles model",
 *   @OA\Property(
 *          property="data",
 *          description="List of roles",
 *          type="array",
 *          @OA\Items(
 *               @OA\Property(
 *                  property="id", description="ID of the role", type="number", example=1
 *               ),
 *               @OA\Property(
 *                  property="role", description="Role", type="string", example="admin"
 *               )
 *        ),
 *   ),
 * )
 *
 */

class RolesRepository extends BaseRepository implements RolesInterface
{

    /**
     * The name of the table
     *
     * @var string
     */
    protected $table = "roles";

    /**
     * The sortable columns
     */
    public $sortables = [
        'id',
        'role',
        'created_at',
        'updated_at'
    ];

    /**
     * The attribute mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'role',
    ];

    /**
     * Return data
     */
    public function index()
    {
        return $this->sort(request())->paginate($this->getPerPage());
    }
}
