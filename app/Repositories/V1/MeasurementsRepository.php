<?php

namespace App\Repositories\V1;

use App\Interfaces\V1\MeasurementsInterface;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *   schema="MeasurementsSchema",
 *   title="Measurements",
 *   description="Measurements model",
 *   @OA\Property(
 *          property="data",
 *          description="List of measurements",
 *          type="array",
 *          @OA\Items(
 *               @OA\Property(
 *                  property="id", description="ID of the measurement", type="number", example=1
 *               ),
 *               @OA\Property(
 *                  property="hip", description="Hip", type="decimal", example="40.01"
 *               ),
 *               @OA\Property(
 *                  property="waist", description="Waist", type="decimal", example="40.01"
 *               ),
 *              @OA\Property(
 *                  property="neck", description="Neck", type="decimal", example="40.01"
 *               ),
 *              @OA\Property(
 *                  property="knee", description="Knee", type="decimal", example="40.01"
 *               ),
 *               @OA\Property(
 *                  property="arm", description="Arm", type="decimal", example="40.01"
 *               ),
 *               @OA\Property(
 *                  property="weight", description="Weight", type="decimal", example="40.01"
 *               ),
 *              @OA\Property(
 *                  property="users",
 *                  description="Associated users",
 *                  type="array",
 *                   @OA\Items(
 *                       @OA\Property(
 *                           property="id", description="ID of the user", type="number", example=1
 *                       ),
 *                       @OA\Property(
 *                           property="email", description="Email", type="string", example="info@example.com"
 *                       ),
 *                   ),
 *              )
 *        ),
 *   ),
 * )
 */

class MeasurementsRepository extends BaseRepository implements MeasurementsInterface
{
    use SoftDeletes;

    /**
     * The name of the table
     *
     * @var string
     */
    protected $table = "measurements";

    /**
     * The sortable columns
     */
    public $sortables = [
        'id',
        'hip',
        'waist',
        'neck',
        'knee',
        'arm',
        'weight',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attribute mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'hip',
        'waist',
        'neck',
        'knee',
        'arm',
        'weight'
    ];

    /**
     * Return associated users
     *
     */
    public function users()
    {
        return $this->hasOne(UsersRepository::class, 'id', 'user_id');
    }

    /**
     * Return data
     */
    public function index()
    {
        return $this
                ->with('users')
                ->sort(request())
                ->paginate($this->getPerPage());
    }
}
