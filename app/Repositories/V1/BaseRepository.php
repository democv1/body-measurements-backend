<?php

namespace App\Repositories\V1;

use App\Traits\Paginate;
use App\Traits\Sort;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * BaseRepository
 *
 * @mixin Builder
 */

class BaseRepository extends Model
{
    use HasFactory, Paginate, Sort;

    /**
     * Store new data
     * @throws \Exception
     */
    public function store($payload): bool
    {
        $this->create($payload);
        return true;
    }

    /**
     * Update a record
     */
    public function patch($id, $payload)
    {
        $model = $this->findOrFail($id);
        $model->update($payload);
        return true;
    }

    /**
     * Delete single record
     */
    public function delete_item(int $id): bool
    {
        try {
            $model = $this->findOrFail($id);
            return $model->delete();
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }
}
