<?php

namespace App\Http\Controllers\Api\V1;

use App\Gateways\V1\UsersGateway;
use App\Http\Controllers\ApiController;

class UsersController extends ApiController
{
    public function __construct(UsersGateway $gateway)
    {
        parent::__construct();
        $this->setGateway($gateway);
    }

    /**
     * @OA\Get(
     *   operationId="202109174700",
     *   path="/api/v1/users",
     *   summary="Return users list",
     *   security={{"bearerAuth":{}}},
     *   tags={"Users"},
     *   @OA\Parameter(ref="#/components/parameters/limit_query"),
     *   @OA\Parameter(ref="#/components/parameters/page_query"),
     *   @OA\Parameter(ref="#/components/parameters/sort_query"),
     *   @OA\Parameter(ref="#/components/parameters/direction_sort"),
     *    @OA\Response(
     *      response=200,
     *      description="Users list",
     *      @OA\JsonContent(ref="#/components/schemas/UsersSchema")
     *    ),
     *    @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *    )
     * )
     *
     * * @OA\Get(
     *   operationId="20211003184600",
     *   path="/api/v1/users/{id}",
     *   summary="Return user with specified ID",
     *   security={{"bearerAuth":{}}},
     *   tags={"Users"},
     *    @OA\Response(
     *      response=200,
     *      description="User data",
     *      @OA\JsonContent(ref="#/components/schemas/UserShowSchema")
     *    ),
     *    @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *    )
     * )
     *
     * @OA\Get(
     *   operationId="20211005065500",
     *   path="/api/v1/users/me",
     *   summary="Return current logged user",
     *   security={{"bearerAuth":{}}},
     *   tags={"Users"},
     *    @OA\Response(
     *      response=200,
     *      description="User data",
     *      @OA\JsonContent(ref="#/components/schemas/UserShowSchema")
     *    ),
     *    @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *    )
     * )
     */

    public function me()
    {
        $id = auth()->user()->id;
        $data = $this->show($id);
        return response()->json($data);
    }
}
