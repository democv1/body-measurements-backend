<?php

namespace App\Http\Controllers\Api\V1;

use App\Gateways\V1\MeasurementsGateway;
use App\Http\Controllers\ApiController;

class MeasurementsController extends ApiController
{
    public function __construct(MeasurementsGateway $gateway)
    {
        parent::__construct();
        $this->setGateway($gateway);
    }

    /**
     * @OA\Get(
     *   operationId="20211001123200",
     *   path="/api/v1/measurements",
     *   summary="Return measurements list for logged user",
     *   security={{"bearerAuth":{}}},
     *   tags={"Measurements"},
     *   @OA\Parameter(ref="#/components/parameters/limit_query"),
     *   @OA\Parameter(ref="#/components/parameters/page_query"),
     *   @OA\Parameter(ref="#/components/parameters/sort_query"),
     *   @OA\Parameter(ref="#/components/parameters/direction_sort"),
     *    @OA\Response(
     *      response=200,
     *      description="Measurements list",
     *      @OA\JsonContent(ref="#/components/schemas/MeasurementsSchema")
     *    ),
     *    @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *    )
     * )
     *
     * @OA\Post(
     *   path="/api/v1/measurements",
     *   summary="Create new measurements",
     *   security={{"bearerAuth":{}}},
     *   tags={"Measurements"},
     *   @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(
     *          @OA\Property(
     *              property="user_id",
     *              description="The user id of the user related to. If omitted, user will be logged user.",
     *              type="number",
     *              example=1,
     *          ),
     *          @OA\Property(
     *              property="hip",
     *              description="The hip",
     *              type="number",
     *              example=80,
     *          ),
     *          @OA\Property(
     *              property="waist",
     *              description="The waist",
     *              type="number",
     *              example=80,
     *          ),
     *          @OA\Property(
     *              property="neck",
     *              description="The neck",
     *              type="number",
     *              example=80,
     *          ),
     *          @OA\Property(
     *              property="knee",
     *              description="The knee",
     *              type="number",
     *              example=80,
     *          ),
     *          @OA\Property(
     *              property="arm",
     *              description="The arm",
     *              type="number",
     *              example=80,
     *          ),
     *          @OA\Property(
     *              property="weight",
     *              description="The weight",
     *              type="number",
     *              example=80,
     *          ),
     *       ),
     *   ),
     *    @OA\Response(
     *      response=201,
     *      description="Success message",
     *      @OA\JsonContent(
     *          @OA\Property(
     *              property="succcess",
     *              description="Success message",
     *              type="string",
     *              example="created"
     *          ),
     *      )
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Error on body",
     *    )
     * )
     *
     * @OA\Delete(
     *   path="/api/v1/measurements/{id}",
     *   summary="Delete single measurement",
     *   security={{"bearerAuth":{}}},
     *   tags={"Measurements"},
     *   @OA\Response(
     *      response=200,
     *      description="Success message",
     *      @OA\JsonContent(
     *          @OA\Property(
     *              property="succcess",
     *              description="Success message",
     *              type="string",
     *              example="deleted"
     *          ),
     *      )
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Error",
     *    )
     * )
     */
}
