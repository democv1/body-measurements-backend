<?php

namespace App\Http\Controllers\Api\V1;

use App\Gateways\V1\ParametersGateway;
use App\Http\Controllers\ApiController;

class ParametersController extends ApiController
{
    public function __construct(ParametersGateway $gateway)
    {
        parent::__construct();
        $this->setGateway($gateway);
    }

    /**
     * @OA\Get(
     *   operationId="20211002130500",
     *   path="/api/v1/parameters",
     *   summary="Return parameters list",
     *   security={{"bearerAuth":{}}},
     *   tags={"Parameters"},
     *   @OA\Parameter(ref="#/components/parameters/limit_query"),
     *   @OA\Parameter(ref="#/components/parameters/page_query"),
     *   @OA\Parameter(ref="#/components/parameters/sort_query"),
     *   @OA\Parameter(ref="#/components/parameters/direction_sort"),
     *    @OA\Response(
     *      response=200,
     *      description="Parameters list",
     *      @OA\JsonContent(ref="#/components/schemas/ParametersSchema")
     *    ),
     *    @OA\Response(
     *      response=401,
     *      description="Unauthenticated",
     *    )
     * )
     *
     * @OA\Post(
     *   path="/api/v1/parameters",
     *   summary="Create new parameter",
     *   security={{"bearerAuth":{}}},
     *   tags={"Parameters"},
     *   @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(
     *          @OA\Property(
     *              property="user_id",
     *              description="The user id of the user related to. If omitted, user will be logged user.",
     *              type="number",
     *              example=1,
     *          ),
     *          @OA\Property(
     *              property="height",
     *              description="The height",
     *              type="number",
     *              example=170.12,
     *          ),
     *          @OA\Property(
     *              property="birthdate",
     *              description="The birthdate",
     *              type="date",
     *              example="1990-12-31",
     *          ),
     *          @OA\Property(
     *              property="sex",
     *              description="The gender of the user (M or F)",
     *              type="string",
     *              example="m",
     *          )
     *       ),
     *   ),
     *    @OA\Response(
     *      response=201,
     *      description="Success message",
     *      @OA\JsonContent(
     *          @OA\Property(
     *              property="succcess",
     *              description="Success message",
     *              type="string",
     *              example="created"
     *          ),
     *      )
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Error on body",
     *    )
     * )
     */

    protected function index()
    {
        $user_id = auth()->user()->id;
        return $this->getGateway()->index($user_id);
    }
}
