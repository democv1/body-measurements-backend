<?php

namespace App\Providers;

use App\Interfaces\V1\MeasurementsInterface;
use App\Interfaces\V1\ParametersInterface;
use App\Interfaces\V1\PingInterface;
use App\Interfaces\V1\RolesInterface;
use App\Interfaces\V1\UsersInterface;
use App\Repositories\V1\MeasurementsRepository;
use App\Repositories\V1\ParametersRepository;
use App\Repositories\V1\PingRepository;
use App\Repositories\V1\RolesRepository;
use App\Repositories\V1\UsersRepository;
use Carbon\Laravel\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            PingInterface::class,
            PingRepository::class
        );

        $this->app->bind(
            UsersInterface::class,
            UsersRepository::class
        );

        $this->app->bind(
            RolesInterface::class,
            RolesRepository::class
        );

        $this->app->bind(
            MeasurementsInterface::class,
            MeasurementsRepository::class,
        );

        $this->app->bind(
            ParametersInterface::class,
            ParametersRepository::class,
        );
    }
}
