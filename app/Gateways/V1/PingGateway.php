<?php

namespace App\Gateways\V1;

use App\Interfaces\V1\PingInterface;

class PingGateway extends BaseGateway
{
    public function __construct(PingInterface $interface)
    {
        $this->setInterface($interface);
    }
}
