<?php

namespace App\Gateways\V1;

use App\Interfaces\V1\UsersInterface;

class UsersGateway extends BaseGateway
{
    public function __construct(UsersInterface $interface)
    {
        $this->setInterface($interface);
    }
}
