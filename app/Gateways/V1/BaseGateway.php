<?php

namespace App\Gateways\V1;

class BaseGateway
{
    private $interface;

    protected function setInterface($interface)
    {
        $this->interface = $interface;
    }

    protected function getInterface()
    {
        return $this->interface;
    }

    public function index()
    {
        return $this->getInterface()->index();
    }

    public function show(int $id)
    {
        return $this->getInterface()->show($id);
    }

    /**
     * Delete a record
     */
    public function delete(int $id)
    {
        return $this->getInterface()->delete_item($id);
    }
}
