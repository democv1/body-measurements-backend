<?php

namespace App\Gateways\V1;

use App\Interfaces\V1\MeasurementsInterface;

class MeasurementsGateway extends BaseGateway
{
    public function __construct(MeasurementsInterface $interface)
    {
        $this->setInterface($interface);
    }

    public function store($payload)
    {
        $id = null;
        if (auth()->user()) {
            $id = auth()->user()->id;
        }
        if (!array_key_exists('user_id', $payload) || $payload['user_id'] === '') {
            $payload['user_id'] = $id;
        }
        return $this->getInterface()->store($payload);
    }
}
