<?php

namespace App\Gateways\V1;

use App\Interfaces\V1\RolesInterface;

class RolesGateway extends BaseGateway
{
    public function __construct(RolesInterface $interface)
    {
        $this->setInterface($interface);
    }
}
