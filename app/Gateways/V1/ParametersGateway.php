<?php

namespace App\Gateways\V1;

use App\Interfaces\V1\ParametersInterface;
use Illuminate\Support\Facades\Validator;

class ParametersGateway extends BaseGateway
{
    public function __construct(ParametersInterface $interface)
    {
        $this->setInterface($interface);
    }

    public function index($user_id = null)
    {
        return $this->getInterface()->index($user_id);
    }

    public function store($payload)
    {
        $rules = [
            'user_id' => 'unique:parameters',
        ];
        $messages = [
            'unique' => 'Parameters need to be unique for single user.'
        ];
        $validator = Validator::make($payload, $rules, $messages);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $id = null;
        if (auth()->user()) {
            $id = auth()->user()->id;
        }
        if (!array_key_exists('user_id', $payload) || $payload['user_id'] === '') {
            $payload['user_id'] = $id;
        }

        // Get how many records exist
        $records = $this->index($id);
        if ($records->count() === 1) {
            return $this->getInterface()->patch($id, $payload);
        }

        return $this->getInterface()->store($payload);
    }
}
