<?php

use Illuminate\Support\Facades\Route;

Route::get('/ping', 'App\Http\Controllers\Api\V1\PingController@index')->name('ping_index');
// Auth
Route::post('/login', 'App\Http\Controllers\Api\V1\AuthController@login')->name('login');
Route::post('/refresh', 'App\Http\Controllers\Api\V1\AuthController@refresh')->name('refresh');
// Users
Route::get('/users', 'App\Http\Controllers\Api\V1\UsersController@index')->name('users_index');
Route::get('/users/me', 'App\Http\Controllers\Api\V1\UsersController@me')->name('users_me');
Route::get('/users/{id}', 'App\Http\Controllers\Api\V1\UsersController@show')->name('users_show');
// Roles
Route::get('/roles', 'App\Http\Controllers\Api\V1\RolesController@index')->name('roles_index');
// Measurements
Route::get('/measurements', 'App\Http\Controllers\Api\V1\MeasurementsController@index')->name('measurements_index');
Route::post('/measurements', 'App\Http\Controllers\Api\V1\MeasurementsController@store')->name('measurements_store');
Route::delete('/measurements/{id}', 'App\Http\Controllers\Api\V1\MeasurementsController@delete')->name('measurements_delete');
// Parameters
Route::get('/parameters', 'App\Http\Controllers\Api\V1\ParametersController@index')->name('parameters_index');
Route::post('/parameters', 'App\Http\Controllers\Api\V1\ParametersController@store')->name('parameters_store');
